EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "AMP-DR"
Date "2021-08-21"
Rev "3.1"
Comp "T5! DIY Audio"
Comment1 "HttPS://t-5.eu/hp/AMP-DR"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H102
U 1 1 5EC33382
P 950 6300
F 0 "H102" H 1050 6346 50  0000 L CNN
F 1 "MountingHole" H 1050 6255 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 950 6300 50  0001 C CNN
F 3 "~" H 950 6300 50  0001 C CNN
	1    950  6300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H101
U 1 1 5EC337D2
P 950 6100
F 0 "H101" H 1050 6146 50  0000 L CNN
F 1 "MountingHole" H 1050 6055 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 950 6100 50  0001 C CNN
F 3 "~" H 950 6100 50  0001 C CNN
	1    950  6100
	1    0    0    -1  
$EndComp
$Comp
L t-5:T5 e101
U 1 1 5EC33CB4
P 1250 5750
F 0 "e101" H 1250 5850 50  0001 C CNN
F 1 "T5" H 1250 5750 50  0001 C CNN
F 2 "T5_w11:T5_w11" H 1250 5750 50  0001 C CNN
F 3 "" H 1250 5750 50  0001 C CNN
	1    1250 5750
	1    0    0    -1  
$EndComp
$Sheet
S 900  1000 2250 750 
U 5F9C2156
F0 "Amplifier" 98
F1 "Amplifier.sch" 98
$EndSheet
$Sheet
S 900  2400 2250 750 
U 5FC0DE84
F0 "PSU" 98
F1 "PSU.sch" 98
$EndSheet
$Comp
L Mechanical:MountingHole H103
U 1 1 6007780D
P 950 6500
F 0 "H103" H 1050 6546 50  0000 L CNN
F 1 "MountingHole" H 1050 6455 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 950 6500 50  0001 C CNN
F 3 "~" H 950 6500 50  0001 C CNN
	1    950  6500
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H104
U 1 1 6007BA0F
P 950 6700
F 0 "H104" H 1050 6746 50  0000 L CNN
F 1 "MountingHole" H 1050 6655 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 950 6700 50  0001 C CNN
F 3 "~" H 950 6700 50  0001 C CNN
	1    950  6700
	1    0    0    -1  
$EndComp
$Comp
L t-5:F1048-KWL-160 CASE101
U 1 1 5FA932A0
P 1200 7350
F 0 "CASE101" H 1200 7500 50  0000 C CNN
F 1 "F1048-KWL-160" H 1200 7400 50  0000 C CNN
F 2 "T5_w11:F1048-KWL-160" H 1200 7350 50  0001 C CNN
F 3 "" H 1200 7350 50  0001 C CNN
	1    1200 7350
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H105
U 1 1 606E2436
P 950 6900
F 0 "H105" H 1050 6946 50  0000 L CNN
F 1 "MountingHole" H 1050 6855 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 950 6900 50  0001 C CNN
F 3 "~" H 950 6900 50  0001 C CNN
	1    950  6900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
