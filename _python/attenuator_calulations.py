#!/usr/bin/python3

N = 96 # E-Range for resistors
DEBUG = True

# functions ######################################################################################

def findNearestER(R_destination):
    global E_RESISTOR_SERIES
    found_R, deviation = 100000, 100000000000
    for E_R in E_RESISTOR_SERIES:
        tmp_deviation = abs(R_destination - E_R)
        if tmp_deviation < deviation:
            deviation = tmp_deviation
            found_R = E_R
    return found_R

def parallelResistors(R_1, R_2, R_3 = None):
    if R_3 is not None:
        return 1 / (1 / R_1 + 1 / R_2 + 1 / R_3)
    return (R_1 * R_2) / (R_1 + R_2)


# MAIN PROGRAM ###################################################################################
from sys import stdin
from math import pi

# E-series Rs ####################################################################################
E_RESISTOR_SERIES = []
for decade in (10, 100, 1000, 10000, 100000, 1000000):
    for m in range(0, N):
        factor = round((10 ** m) ** (1 / N), 2)
        E_RESISTOR_SERIES.append(factor * decade)

print("Desired Gain Factor (G): ", end='', flush=True)
G = float(stdin.readline())
print("Desired Servo cutoff frequency (Fg) [Hz]: ", end='', flush=True)
Fg = float(stdin.readline())
print("Desired Servo range (Vr) [V]: ", end='', flush=True)
Vr = float(stdin.readline())

R210 = findNearestER(2000 / (G - 1))
G = (R210 + 2000) / R210
print("R210: %0.1fOhm" % R210)
print("Actual Gain Factor (G): %0.2f" % G)

ATTENUATION_RANGE = 18 / (1.1 * Vr) / G, 18 / (0.9 * Vr) / G

foundResult = { 'r': 10000000, 'r205': None, 'r206': None, 'r202': None, 'att': None }
for r202 in E_RESISTOR_SERIES:
    att = (r202 + 1000) / 1000
    if ATTENUATION_RANGE[0] <= att <= ATTENUATION_RANGE[1]:
        for r205 in E_RESISTOR_SERIES:
            for r206 in E_RESISTOR_SERIES:
                r = parallelResistors(r206, r205, r202)
                if abs(r - 1000) < abs(foundResult["r"] - 1000):
                    foundResult = { 'r': r, 'r206': r206, 'r205': r205, 'r202': r202, 'att': att }

print("Servo Attenuation: %3.2f\nActual Rparallel: %3.7fkOhm (vs 1.0kOhm)\nR202: %3.3fkOhm\nR05: %3.3fkOhm\nR06: %0.3fkOhm" % (
    foundResult["att"],
    parallelResistors(foundResult["r205"], foundResult["r206"], foundResult["r202"]) / 1000,
    foundResult["r202"] / 1000,
    foundResult["r205"] / 1000,
    foundResult["r206"] / 1000))

print("C201: %3.2fnF" % (1 / Fg / pi / 2 / 2200000 * 1000000000 / foundResult["att"]))

print("\nPress ENTER key to continue...");
stdin.readline()
